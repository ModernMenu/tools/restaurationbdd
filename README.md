### Restauration d'un dump sql sur un container docker postgreSQL

Se connecter sur pgadmin et supprimer les tables existantes:

Clic droit sur la table et faire un Delete\Drop
Commencer par la table article puis catégorie

![Suppresion Table](deletetable.png)

Se placer dans le dossier dans lequel se trouve le dump et lancer un terminal tel que Git Bash

Faire un docker ps pour récuperer l'ID du container postgresql

```bash
$ docker ps
CONTAINER ID   IMAGE            COMMAND                  CREATED             STATUS             PORTS                           NAMES
8485e9f4bada   dpage/pgadmin4   "/entrypoint.sh"         About an hour ago   Up About an hour   443/tcp, 0.0.0.0:5050->80/tcp   pgadmin4_container
957f3481ab80   postgres         "docker-entrypoint.s…"   About an hour ago   Up About an hour   0.0.0.0:5432->5432/tcp          pg_container

```

Lancer la commande de restauration en mettant l'ID de votre container

```bash
$ docker ps
cat dump.sql | docker exec -i 957f3481ab80 psql -U root -d restaurant
```

Vous verrez défiler les différentes étapes de restauration et s'il n'y a pas d'erreur, tout sera OK.

![Restauration Table](restauration.png)